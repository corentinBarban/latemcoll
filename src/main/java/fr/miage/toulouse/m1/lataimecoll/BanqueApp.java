/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.toulouse.m1.lataimecoll;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.services.ServiceBanqueRemote;
import com.toulouse.miage.m1.corentinbarban.lataimeshared.Position;
import exceptions.CompteInexistantException;
import exceptions.MontantInvalidException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.DefaultListModel;

/**
 *
 * @author mutatep
 */
public class BanqueApp extends javax.swing.JFrame {

    /**
     * Creates new form BanqueApp
     */
    private ServiceBanqueRemote collabRemoteSvc;

    public BanqueApp() throws NamingException {
        initComponents();

        //Contexte de nommage pour l'accès à l'annuaire JNDI du serveur Glassfish
        Properties jNDIProperties = new Properties();
        jNDIProperties.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
        jNDIProperties.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
        jNDIProperties.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
        InitialContext namingContext = new InitialContext(jNDIProperties);
        //Récupération d'un accès à l'EJB distant
        String uri = "java:global/LaTAIME-ear/LaTAIME-ejb-1.0/ServiceBanque!com.toulouse.miage.m1.corentinbarban.LaTAIME.services.ServiceBanqueRemote";
        this.collabRemoteSvc = (ServiceBanqueRemote) namingContext.lookup(uri);

        //Remplir la liste des clients
        DefaultListModel model = new DefaultListModel();
        for (int idCompte : this.collabRemoteSvc.getComptes()) {
            model.addElement(String.valueOf(idCompte));
        }
        this.jList1.setModel(model);

        this.jLabel2.setVisible(false);
        this.jTextField1.setVisible(false);
        this.solde.setVisible(false);
        this.soldeL.setVisible(false);
        this.annuler.setVisible(false);
        this.valider.setVisible(false);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        consulter = new javax.swing.JButton();
        crediter = new javax.swing.JButton();
        listBanque = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        solde = new javax.swing.JLabel();
        soldeL = new javax.swing.JLabel();
        crediterT = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        valider = new javax.swing.JButton();
        annuler = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        consulter.setText("Consulter position");
        consulter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consulterActionPerformed(evt);
            }
        });

        crediter.setText("Créditer");
        crediter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crediterActionPerformed(evt);
            }
        });

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listBanque.setViewportView(jList1);

        solde.setText("jLabel1");

        soldeL.setText("Solde : ");

        crediterT.setText("Somme à créditer :");

        valider.setText("Valider");
        valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validerActionPerformed(evt);
            }
        });

        annuler.setText("Annuler");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(crediter)
                        .addGap(29, 29, 29))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(consulter)
                            .addComponent(listBanque, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(42, 42, 42)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addContainerGap())
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(annuler)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(soldeL)
                                        .addComponent(crediterT)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(solde, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(21, 21, 21))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(valider)
                                        .addGap(88, 88, 88))))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(58, Short.MAX_VALUE)
                        .addComponent(listBanque, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(solde)
                            .addComponent(soldeL))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(crediterT)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(valider)
                            .addComponent(annuler))))
                .addGap(18, 18, 18)
                .addComponent(crediter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(consulter)
                .addGap(29, 29, 29))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void consulterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consulterActionPerformed
        Integer idCompte = Integer.parseInt(this.jList1.getSelectedValue());
        this.jTextField1.setVisible(false);
        this.solde.setVisible(true);
        this.soldeL.setVisible(true);
        this.annuler.setVisible(false);
        this.valider.setVisible(false);
        this.crediterT.setVisible(false);
        try {
            Position p = collabRemoteSvc.consulterPosition(idCompte);
            this.solde.setText(String.valueOf(p.getSolde()));
        } catch (CompteInexistantException ex) {
            Logger.getLogger(BanqueApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_consulterActionPerformed

    private void crediterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crediterActionPerformed
        this.jTextField1.setVisible(true);
        this.solde.setVisible(true);
        this.soldeL.setVisible(true);
        this.annuler.setVisible(true);
        this.valider.setVisible(true);
        this.crediterT.setVisible(true);
        try {
            Integer idCompte = Integer.parseInt(this.jList1.getSelectedValue());
            Position p = collabRemoteSvc.consulterPosition(idCompte);
            this.solde.setText(String.valueOf(p.getSolde()));
        } catch (CompteInexistantException ex) {
            Logger.getLogger(BanqueApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_crediterActionPerformed

    private void validerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validerActionPerformed
        this.jTextField1.setVisible(true);
        this.solde.setVisible(true);
        this.soldeL.setVisible(true);
        this.annuler.setVisible(true);
        this.valider.setVisible(true);
        this.crediterT.setVisible(true);
        
        try {
            Integer idCompte = Integer.parseInt(this.jList1.getSelectedValue());

            Double montant = Double.parseDouble(this.jTextField1.getText());
            this.collabRemoteSvc.crediter(idCompte, montant);
            Position p = collabRemoteSvc.consulterPosition(idCompte);
            this.solde.setText(String.valueOf(p.getSolde()));
        } catch (CompteInexistantException | MontantInvalidException ex) {
            Logger.getLogger(BanqueApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.jTextField1.setText("");
    }//GEN-LAST:event_validerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BanqueApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BanqueApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BanqueApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BanqueApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new BanqueApp().setVisible(true);
                } catch (NamingException ex) {
                    Logger.getLogger(BanqueApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton annuler;
    private javax.swing.JButton consulter;
    private javax.swing.JButton crediter;
    private javax.swing.JLabel crediterT;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList<String> jList1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JScrollPane listBanque;
    private javax.swing.JLabel solde;
    private javax.swing.JLabel soldeL;
    private javax.swing.JButton valider;
    // End of variables declaration//GEN-END:variables
}
